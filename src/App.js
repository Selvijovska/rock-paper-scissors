import Score from "./components/score/Score";
import Rules from "./components/rules/Rules";
import Game from "./components/game/Game";

function App() {
    return (
        <div className="App">
            <Score />
            <Game />
            <Rules />
        </div>
    );
}

export default App;
