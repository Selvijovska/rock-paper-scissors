import rock from "../images/icon-rock.svg";
import paper from "../images/icon-paper.svg";
import scissors from "../images/icon-scissors.svg";

const choices = [
    {
        choice: "paper",
        image: paper,
        num: 1,
    },
    {
        choice: "rock",
        image: rock,
        num: 0,
    },
    {
        choice: "scissors",
        image: scissors,
        num: 2,
    },
];

export default choices;
