import React from "react";
import House from "../house/House";
import Player from "../player/Player";
import Result from "../result/Result";
import { useSelector } from "react-redux";

function Game() {
    const playerPick = useSelector((state) => state.reducers.playerPick);
    const housePick = useSelector((state) => state.reducers.housePick);

    return (
        <div>
            <div className="game-wrapper">
                <Player />

                {playerPick && housePick ? <Result /> : null}

                <House />
            </div>
        </div>
    );
}

export default Game;
