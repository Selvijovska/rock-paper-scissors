import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import choices from "../choices";
import * as actions from "../../redux/actions/actions";

function Result() {
    const player = useSelector((state) => state.reducers.playerPick);
    const house = useSelector((state) => state.reducers.housePick);
    const gameResult = useSelector((state) => state.reducers.gameResult);
    const dispatch = useDispatch();

    const findPlayerNum = choices.find((item) => {
        return item.choice === player;
    }).num;

    const findHouseNum = choices.find((item) => {
        return item.choice === house;
    }).num;

    const handleRestart = useCallback(() => {
        dispatch(actions.restartGame());
    }, [dispatch]);

    useEffect(() => {
        if ((findPlayerNum + 1) % 3 === findHouseNum) {
            dispatch(actions.loseAction());
        } else if (findPlayerNum === findHouseNum) {
            dispatch(actions.drawAction());
        } else {
            dispatch(actions.winAction());
        }
    }, [dispatch, findPlayerNum, findHouseNum]);

    return (
        <div>
            {player && house ? (
                <div className="result">
                    {gameResult === "draw" ? (
                        <div>
                            <p className="result-animation">{gameResult}</p>
                            <button
                                className="play-again-btn"
                                onClick={handleRestart}
                            >
                                play again
                            </button>
                        </div>
                    ) : (
                        <div>
                            <p className="result-animation">you {gameResult}</p>
                            <button
                                className="play-again-btn"
                                onClick={handleRestart}
                            >
                                play again
                            </button>
                        </div>
                    )}
                </div>
            ) : null}
        </div>
    );
}

export default Result;
