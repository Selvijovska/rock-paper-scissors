import { useCallback, useState } from "react";
import rules from "../../images/image-rules.svg";
import close from "../../images/icon-close.svg";

function Rules() {
    const [showImage, setShowImage] = useState(false);
    const handleShowImage = useCallback(() => {
        setShowImage(!showImage);
    }, [showImage]);
    return (
        <div className="rules-container">
            <button onClick={handleShowImage} className="rules-btn">
                RULES
            </button>
            {showImage ? (
                <div className="rules">
                    <div>
                        <p>RULES</p>
                        <button onClick={handleShowImage}>
                            <img src={close} alt="close"></img>
                        </button>
                    </div>
                    <img className="rules-img" src={rules} alt="rules"></img>
                </div>
            ) : null}
        </div>
    );
}

export default Rules;
