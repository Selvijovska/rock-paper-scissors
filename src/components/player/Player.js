import { useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import triangle from "../../images/bg-triangle.svg";
import choices from "../choices";
import * as actions from "../../redux/actions/actions";

const Player = () => {
    const dispatch = useDispatch();
    const playerPick = useSelector((state) => state.reducers.playerPick);
    const gameResult = useSelector((state) => state.reducers.gameResult);
    const [playerImg, setPlayerImg] = useState();

    const handleChoice = useCallback(
        (choice) => {
            const playerImage = choices.find(
                (item) => item.choice === choice
            ).image;
            setPlayerImg(playerImage);
            dispatch(actions.playerPick(choice));
        },
        [dispatch]
    );

    return (
        <div>
            {playerPick ? (
                <div className="player-wrapper">
                    <p className="pick">you picked </p>
                    <img
                        className={`picked-image img-${playerPick} ${
                            gameResult === "win" ? `winner-${playerPick}` : ""
                        }`}
                        alt=""
                        src={playerImg}
                    />
                </div>
            ) : (
                <div className="triangle-container">
                    <img
                        className="triangle"
                        src={triangle}
                        alt="triangle"
                    ></img>
                    <ul>
                        {choices.map((item, index) => (
                            <li
                                className={`${item.choice} choice`}
                                key={index}
                                onClick={() => handleChoice(item.choice)}
                            >
                                <img alt="" src={item.image} />
                            </li>
                        ))}
                    </ul>
                </div>
            )}
        </div>
    );
};

export default Player;
