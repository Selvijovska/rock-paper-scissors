import { useSelector, useDispatch } from "react-redux";
import { useEffect, useMemo, useRef } from "react";
import choices from "../choices";
import * as actions from "../../redux/actions/actions";

function House() {
    const dispatch = useDispatch();
    const house = useSelector((state) => state.reducers.housePick);
    const player = useSelector((state) => state.reducers.playerPick);
    const gameResult = useSelector((state) => state.reducers.gameResult);
    const isFirstRun = useRef(true);

    useEffect(() => {
        if (isFirstRun.current) {
            isFirstRun.current = false;
            return;
        }
        setTimeout(() => {
            dispatch(actions.housePick());
        }, 1000);

        isFirstRun.current = true;
    }, [player, dispatch]);

    const houseImage = useMemo(() => {
        if (house) {
            return choices.find((item) => item.choice === house).image;
        }
        return "";
    }, [house]);

    return (
        <div>
            {!player ? null : (
                <div>
                    {player && !house ? (
                        <div className="house-wrapper">
                            <p className="pick">the house is picking...</p>
                            <div className="flip-wrapper">
                                <div>
                                    <p className="wait-circle"></p>
                                </div>
                            </div>
                        </div>
                    ) : (
                        <div className="house-wrapper">
                            <p className="pick">the house picked </p>
                            <img
                                className={`picked-image img-${house} ${
                                    gameResult === "lose"
                                        ? `winner-${house}`
                                        : ""
                                }`}
                                alt=""
                                src={houseImage}
                            />
                        </div>
                    )}{" "}
                </div>
            )}
        </div>
    );
}

export default House;
