import { useSelector } from "react-redux";
import logo from "../../images/logo.svg";

function Score() {
    const score = useSelector((state) => state.reducers.gameScore);
    return (
        <div className="score-wrapper">
            <img src={logo} alt="logo"></img>
            <div className="score-container">
                <p className="score-text">score</p>
                <p className="score-num">{score}</p>
            </div>
        </div>
    );
}

export default Score;
