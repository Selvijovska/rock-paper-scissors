export const playerPick = (payload) => {
    return {
        type: "PLAYER_PICK",
        payload: payload,
    };
};

export const housePick = () => {
    return {
        type: "HOUSE_PICK",
    };
};

export const winAction = () => {
    return {
        type: "WIN",
    };
};

export const loseAction = () => {
    return {
        type: "LOSE",
    };
};

export const drawAction = () => {
    return {
        type: "DRAW",
    };
};

export const startGame = () => {
    return {
        type: "START_GAME",
    };
};

export const restartGame = () => {
    return {
        type: "RESTART_GAME",
    };
};
