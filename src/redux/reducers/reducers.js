import choices from "../../components/choices";

const initialState = {
    playerPick: "",
    housePick: "",
    gameResult: "",
    gameScore: 0,
};

const reducers = (state = initialState, action) => {
    switch (action.type) {
        case "WIN":
            return {
                ...state,
                gameScore: state.gameScore + 1,
                gameResult: "win",
            };

        case "LOSE":
            return {
                ...state,
                gameScore: state.gameScore - 1,
                gameResult: "lose",
            };

        case "DRAW":
            return {
                ...state,
                gameScore: state.gameScore,
                gameResult: "draw",
            };

        case "PLAYER_PICK":
            return {
                ...state,
                playerPick: action.payload,
            };

        case "RESTART_GAME":
            return {
                ...initialState,
                gameScore: state.gameScore,
            };

        case "HOUSE_PICK":
            return {
                ...state,
                housePick:
                    choices[Math.floor(Math.random() * choices.length)].choice,
            };

        default:
            return {
                ...initialState,
            };
    }
};

export default reducers;
